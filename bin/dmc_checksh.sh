#!/bin/bash
# SPDX-FileCopyrightText: 2021 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# Script para revisar con shellharden los scripts .sh de /usr/local/{bin,sbin}
set -euo pipefail

for fichero in /usr/local/{bin,sbin}/*.sh; do
  if ! shellharden --check "$fichero"; then
    echo "shellharden $fichero"
  fi
done
