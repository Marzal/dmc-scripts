#!/bin/bash
# SPDX-FileCopyrightText: 2021 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# Script para mostrar información de stack gŕafico X11/Wayland
#2025-01-29
#set -euo pipefail

HL='\033[1;32m' # Bold Green
WL='\033[1;33m' # Bold Yellow
NL='\033[0m'    # Text Reset

f_pinta() {
  [ "$1" = "W" ] && SC=$WL
  [ "$1" = "H" ] && SC=$HL
  echo -e "${SC} $2 ${NL}"
}

if [ -z "$1" ]; then
  logfile=/var/log/Xorg.0.log
else
  logfile="$1"
fi

f_pinta H "El ultimo driver cargado en $logfile es:"
sed -n 's@.* Loading .*/\(.*\)_drv.so@\1@p' "$logfile" |
  while read -r driver; do
    if ! grep -q "Unloading $driver" "$logfile"; then
      f_pinta W "$driver"
      break
    fi
  done

f_pinta H "\nVideo adapters:"
ls -la /sys/class/drm

f_pinta H "\nxrandr:"
xrandr

f_pinta H "\nxrandr: properties for each  output"
xrandr --prop

f_pinta H "\nxrandr: Report information about the providers available"
xrandr --listproviders

# https://wiki.archlinux.org/index.php/Variable_refresh_rate
f_pinta H "\nComprobando FreeSync"
xrandr --props | grep -iE "vrr|TearFree"
grep -Ri Variable /etc/X11/*

f_pinta H "\nLa variable XDG_SESSION_TYPE es:"
echo -n "$XDG_SESSION_TYPE"

f_pinta H "\nLa variable WAYLAND_DISPLAY es:"
echo -n "$WAYLAND_DISPLAY"

f_pinta H "\nloginctl show-session 1 -p Type:"
loginctl show-session 1 -p Type
#loginctl show-session $(awk '/tty/ {print $1}' <(loginctl)) -p Type | awk -F= '{print $2}'

f_pinta H "\nLog .local/share/sddm/wayland-session.log"
cat .local/share/sddm/wayland-session.log

f_pinta H "\nList XWayland applications:"
xlsclients

f_pinta H "\nSesiones X11 disponibles:"
find /usr/share/xsessions/ -type f

f_pinta H "\nSesiones Wayland disponibles:"
find /usr/share/wayland-sessions/ -type f

f_pinta H "\nSesiones en /etc/sddm.conf.d/ autologin:"
[ -d /etc/sddm.conf.d ] && grep Session /etc/sddm.conf.d/*

f_pinta H "\nAceleración por HW: vainfo"
vainfo


f_pinta H "\nImplementaciones Vulkan instaladas en el sistema:"
ls -l /usr/share/vulkan/icd.d/
env| grep -iE 'VULKAN|VK|AMD'
echo "vulkaninfo --summary"




#https://www.x.org/wiki/RadeonFeature/
#https://wiki.archlinux.org/index.php/Xorg#Installation
#https://wiki.archlinux.org/index.php/AMDGPU
#https://jlk.fjfi.cvut.cz/arch/manpages/man/modesetting.4
#https://fedoraproject.org/wiki/How_to_debug_Wayland_problems
#https://wiki.archlinux.org/title/Vulkan


#get-edid
# Para Xorg
#lspci | grep VGA ; lsmod | grep "kms\|drm" ; find /dev -group video ; cat /proc/cmdline ; find /etc/modprobe.d/; ls -l /etc/X11/xorg.conf* ; glxinfo | grep -i "vendor\|rendering" ; grep -i LoadModule /var/log/Xorg.0.log
