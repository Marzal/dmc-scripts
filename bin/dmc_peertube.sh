#!/bin/bash
# SPDX-FileCopyrightText: 2021 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# Script para automatizar Peertube

set -eEuo pipefail

VERSION=2022-03-08

# Get helper functions
readonly bindir=/usr/local/bin
readonly funciones=dmc_bashelper.sh
#funciones=/usr/local/bin/dmc_bashhelper.sh
if [[ -s $funciones && -r $funciones ]]; then
    # the helper script takes precedence in the same folder
    # shellcheck source=dmc_bashelper.sh
    source $funciones
else
    # shellcheck source=../bin/dmc_bashelper.sh
    [[ -s $bindir/$funciones && -r $bindir/$funciones ]] && source $bindir/$funciones || (
        echo "Este script necesita $funciones"
        exit 33
    )
fi

f_existe_comando jq || exitWithErrMsg 'ERROR: Dependencia jq no encontrada'

show_help() {
    echo "Peertube cli by @DMarzal"
    echo
    echo "Usage: peertube.sh -a profile [OPTIONS]"
    echo
    echo " -a | --account               - Peertube's profile - Mandatory first"
    echo " -c | --channel               - Channel to work with"
    echo " -d | --desc                  - Video description"
    echo " -u | --upload                - video to upload"
    echo " -V | --version               - print version of this script"
    echo " -h | --help                  - print this help"
    echo " -C | --canales               - Get channels from ACCOUNT"
    echo " -L | --licencias             - Get licences avaiables"
}

## AUTH
get_token() {
    [[ $verbose -ge 1 ]] && f_pinta H "DEBUG: Pidiendo token"
    client_id=$(curl -s "${API_PATH}/oauth-clients/local" | jq -r ".client_id")
    client_secret=$(curl -s "${API_PATH}/oauth-clients/local" | jq -r ".client_secret")
    TOKEN=$(curl -s "${API_PATH}/users/token" \
        --data client_id="${client_id}" \
        --data client_secret="${client_secret}" \
        --data grant_type=password \
        --data response_type=code \
        --data username="${USERNAME}" \
        --data password="${PASSWORD}" |
        jq -r ".access_token")
    f_establece_parametros "$LOCALCONF" TOKEN= "$TOKEN"
    OA="Authorization: Bearer $TOKEN"
}

test_token() {
    error=$(curl -s "${API_PATH}/users/me/notifications?unread=true" \
        -H "$OA" --max-time 6000 | jq ".error")
    if [ "${error}" != "null" ]; then
        [[ $verbose -ge 0 ]] && f_pinta W "Token con error ($error), lo pedimos"
        get_token
    fi
}

check_token() {
    if [ -z "${TOKEN:-}" ]; then
        [[ $verbose -ge 0 ]] && f_pinta W "No hay token, lo pedimos"
        get_token
    else
        [[ $verbose -ge 1 ]] && f_pinta H "Ya tenemos token"
        OA="Authorization: Bearer $TOKEN"
        test_token
    fi
}

get_videos() {
    # video languages licenses
    [[ $verbose -ge 1 ]] && echo "Getting $1"
    curl -s "${API_PATH}/videos/$1" \
        -H "$OA" --max-time 6000 | jq
}
#get_videos licences

get_accounts() {
    #  video-channels
    curl -s "${API_PATH}/accounts/$USERNAME/$1" \
        -H "$OA" --max-time 6000
}
#get_accounts video-channels | jq '.data[] | .id, .name'

do_upload() {
    NAME="${FILE_PATH%.*}"
    NAME="${NAME#*/}"
    [[ $verbose -ge 0 ]] && echo "Uploading ${FILE_PATH}"
    curl -s "${API_PATH}/videos/upload" \
        -H "${OA}" \
        --max-time 6000 \
        --form videofile=@"${FILE_PATH}" \
        --form channelId="${channel}" \
        --form name="${NAME-$FILE_PATH}" \
        --form category=15 \
        --form licence="${LICENCIA-3}" \
        --form description="${DESCRIPTION-peertube-cli}" \
        --form language=es \
        --form privacy="${cpriv-2}" \
        --form tags="$TAG1" \
        --form tags="$TAG2" \
        --form tags="$TAG3" \
        --form tags="$TAG4" |
        jq
}

TOKEN=
FILE_PATH=
verbose=0

if [[ ${1-} =~ ^(-a|--acount)$ ]]; then
    [[ -n ${2:-} ]] && CUENTA="${2}" || exitWithErrMsg 'ERROR: -a PROFILE is mandatory as first parameter'
    f_get_config "$HOME/.config/peertube-cli/peertube_$CUENTA.conf"
    check_token
    shift 2 || exitWithErrMsg '-a PROFILE is mandatory as first parameter'
else
    show_help
    exitWithErrMsg '-a PROFILE is mandatory as first parameter'
    exit 1
fi

set +u
# https://mywiki.wooledge.org/BashFAQ/035#Manual_loop
while :; do
    case $1 in
    -h | -\? | --help)
        show_help # Display a usage synopsis.
        exit
        ;;
    -V | --version)
        echo "Ver: $VERSION"
        exit
        ;;
    -v | --verbose)
        verbose=$((verbose + 1)) # Each -v adds 1 to verbosity.
        ;;
    -d | --desc)
        if [ "$2" ]; then
            DESCRIPTION="$2"
            shift
        else
            exitWithErrMsg 'ERROR: "-d/--desc" requires a non-empty option argument.'
        fi
        ;;
    -c | --channel)
        if [ "$2" ]; then
            channel="$2"
            shift
        else
            exitWithErrMsg 'ERROR: "-c/--channel" requires a non-empty option argument.'
        fi
        ;;
    -u | --upload)
        if [ "$2" ]; then
            FILE_PATH=$2
            shift
        else
            exitWithErrMsg 'ERROR: "-u/--upload" requires a non-empty option argument.'
        fi
        do_upload
        ;;
    --upload=?*)
        FILE_PATH=${1#*=} # Delete everything up to "=" and assign the remainder.
        ;;
    --upload=) # Handle the case of an empty --upload=
        exitWithErrMsg 'ERROR: "--upload" requires a non-empty option argument.'
        ;;
    -C | --canales | --channels)
        get_accounts video-channels | jq '.data[] | .id, .name'
        break
        ;;
    -L | --licencias | --licences)
        get_videos licences
        break
        ;;
    --) # End of all options.
        shift
        break
        ;;
    -?*)
        printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
        ;;
    *) # Default case: No more options, so break out of the loop.
        break
        ;;
    esac

    shift
done

printf "\nFIN: ¡Gracias por compartir!\n\n"
# https://docs.joinpeertube.org/api-rest-reference.html
# https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/api/openapi.yaml
# https://www.artificialworlds.net/blog/2021/04/30/uploading-to-peertube-from-the-command-line/
