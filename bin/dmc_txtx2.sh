#!/bin/bash

filename="$1"
# check if file exists
if [ ! -f "$filename" ]; then
    echo "File not found!"
    exit 1
fi

fileo="${filename%.*}.txt"
file32="${filename%.*}_32.txt"
filet="${filename%.*}_temp.txt"

cp "$filename" "$file32"
mv "$filename" "$filet"

sed "s/^[ \t]*//" -i "$filet"

line_number=1
while IFS= read -r line; do
    if (( line_number % 2 == 1 )); then
        printf "%s " "$line"
    else
        echo "$line "
    fi
    ((line_number++))
done < "$filet" > "$fileo"	# filet is a temporary file
rm -f "$filet"

dmc_wfix.sh "$fileo"

#echo "meld $filename $fileo"
