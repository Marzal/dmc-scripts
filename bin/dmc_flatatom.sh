#!/bin/sh

# Script para poder lanzar shellcheck en un "jaula" de flatpak

# --sandbox and --no-network do not work
flatpak-spawn --host shellcheck "$@"
