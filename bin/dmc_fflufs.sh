#!/bin/bash
# SPDX-FileCopyrightText: 2023 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# Wrapper para normalizar audio con loudnorm de ffmpeg

set -euo pipefail
# http://redsymbol.net/articles/unofficial-bash-strict-mode/

HL='\033[1;32m' # Bold Green
WL='\033[1;33m' # Bold Yellow
NL='\033[0m'    # Text Reset

# Formato
SALIDA="mp3 ogg"
kbps="${2:-128}"

# Targets
I='-16'        # -24.0 Por defecto en ffmpeg
TP="${3:--1}"  # -2.0 - EBU <= -2
LRA="${4:-20}" # 7.0  - EBU <= 7
# Si es menor que la actual se usa Dynamic en vez de linear, lo cual pasa casi siempre


usage() {
    echo -e "${WL}Este script necesita al menos un parametro.${NL} Uso:
    ${0##*/} ${HL}fichero ${HL}[tp] [lra]${NL}
Por defecto ${HL}I=${WL}$I $HL TP=${WL}$TP $HL LRA=${WL}$LRA ${NL}"
}

if [ $# -lt 1 ]; then
    usage # Comprobar que invocamos bien el script
    exit 2
fi

INPUT=$(basename "$1")                      # Nombre del fichero
LOGNAME="${LRA}${TP}${I}_${INPUT%.*}.stats" # Nombre para el log
#LOG1="/tmp/${INPUT%.*}_$FECHA.log"
#LOG1="/tmp/$LOGNAME" # Log temporal de la primera paasa que descartamos
LOG1="${LOGNAME%.*}_inicial.stats" # Log temporal de la primera paasa que descartamos
#FECHA="$(date +%F_%H%M)"
METAFILE='metadata.ffmpeg'
METADATA=""
[ -s $METAFILE ] && METADATA="-i $METAFILE -map_metadata 1"

## Deleting trailing zeros alternatives
#HZ=$(ffprobe "$INPUT" -show_streams 2>&1 | awk -F'=' '/sample_rate/ {print $2}' | rev | awk '{print $1*1}'|rev)
#HZ=$(ffprobe "$INPUT" -show_streams 2>&1 | awk -F'=' '/sample_rate/ {print $2}' | sed 's/[0]*$//')
# Sacamos la frecuencia del original
HZ=$(ffprobe "$INPUT" -show_streams 2>&1 | awk -F'=' '/sample_rate/ {print $2}')

echo -e "$HL Procesando audio de ${WL}$HZ Hz ${HL}con los siguientes parametros${NL}"
echo -e "$HL  I=${WL}$I $HL TP=${WL}$TP $HL LRA=${WL}$LRA ${NL}con el fichero de log en $LOG1"

# Primera pasada para obtener los datos
ffmpeg -hide_banner -i "$INPUT" -af "loudnorm=I=$I:tp=$TP:LRA=$LRA:dual_mono=true:print_format=json" -f null - 2>|"$LOG1"

# Parseamos la salida
m_I=$(grep -Po '"'"input_i"'"\s*:\s*"\K([^"]*)' "$LOG1")
m_TP=$(grep -Po '"'"input_tp"'"\s*:\s*"\K([^"]*)' "$LOG1")
m_LRA=$(grep -Po '"'"input_lra"'"\s*:\s*"\K([^"]*)' "$LOG1")
m_TH=$(grep -Po '"'"input_thresh"'"\s*:\s*"\K([^"]*)' "$LOG1")
m_o=$(grep -Po '"'"target_offset"'"\s*:\s*"\K([^"]*)' "$LOG1")

echo -e "$HL\n Dando segunda pasada con los siguientes parametros:"
echo -e "$HL  input_i=${WL}$m_I $HL input_tp=${WL}$m_TP $HL input_lra=${WL}$m_LRA $HL input_thresh=${WL}$m_TH $HL target_offset=${WL}$m_o ${NL}"

if [[ $(echo "$LRA > $m_LRA" | bc) -eq 0 ]]; then
    echo -e "${WL}-->La normalización no podrá hacerse con escalado lineal ya que el LRA objetivo ${HL}$LRA${WL} es menor que el actual ${HL}$m_LRA${NL}"
fi

for EXT in $SALIDA; do
    echo "${HL}$EXT${NL} $kbps:"
    #[[ $EXT == ogg ]] && sed -i 's/^;CHAPTER/CHAPTER/' $METAFILE; kbps=$((kbps-32)) || echo -e "${WL}OGG: No hay CHAPTER en $METAFILE${NL}"
    #[[ $EXT == mp3 ]] && sed -i 's/^CHAPTER/;CHAPTER/' $METAFILE || echo -e "${WL}MP3: No hay CHAPTER en $METAFILE${NL}"
    if [[ $EXT == ogg ]]; then
        sed -i 's/^;CHAPTER/CHAPTER/' $METAFILE || echo -e "${WL}OGG: No hay CHAPTER en $METAFILE ${NL}"
        kbps=$((kbps-32))
    elif [[ $EXT == mp3 ]] ; then 
        sed -i 's/^CHAPTER/;CHAPTER/' $METAFILE || echo -e "${WL}MP3: No hay CHAPTER en $METAFILE ${NL}"
    else
        echo -e "$WL\n EXTENSION DESCONOCIDA"
    fi
    ffmpeg -hide_banner -i "$INPUT" $METADATA \
        -af "loudnorm=I=$I:tp=$TP:LRA=$LRA:measured_I=$m_I:measured_TP=$m_TP:measured_LRA=$m_LRA:measured_thresh=$m_TH:offset=$m_o:dual_mono=true:linear=true:print_format=json" \
        -ar "$HZ" -b:a "${kbps}k" \
        "${LRA}${TP}${I}_${INPUT%.*}.$EXT" 2>&1 | tee "$LOGNAME"
done

echo -e "$HL\n Para generar .wav a 16khz en mono para whisper cpp{$NL}"
echo ffmpeg -i "$INPUT" -ar 16000 -ac 1 -c:a pcm_s16le "${INPUT%.*}_16k.wav"
echo "/usr/bin/whisper.cpp --model /home/dmc/opt/whisper/ggml-large-v3-turbo.bin -t 5 -ovtt -osrt -oj -otxt -l es -ml 32 -sow -f '${INPUT%.*}_16k.wav'"

## Docu
# https://marzal.gitlab.io/mundolibre/docs/multimedia/ffmpeg/#normalizaci%C3%B3n
# https://marzal.gitlab.io/mundolibre/docs/multimedia/audacity/#loudness-normalization

# http://ffmpeg.org/ffmpeg-filters.html#loudnorm
# https://forums.freebsd.org/threads/peer-review-for-ffmpeg-shell-scripts.72932/
# https://forum.audacityteam.org/viewtopic.php?t=107569&start=10

## Spec
# https://tech.ebu.ch/publications/r128/
# https://tech.ebu.ch/publications/r128s2

## Info
# https://www.masteringthemix.com/pages/mixing-with-levels#LoudnessRange
