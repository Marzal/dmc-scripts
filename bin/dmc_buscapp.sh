#!/bin/bash
# SPDX-FileCopyrightText: 2021 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# Script para buscar paquetes en diferentes distribuciones
set -euo pipefail

HL='\033[1;32m' # Bold Green
WL='\033[1;33m' # Bold Yellow
NL='\033[0m'    # Text Reset

f_get_config() {
  # Si hay un fichero de configuracion lo leemos que sobreescriba/setee las Variables
  # https://stackoverflow.com/questions/16571739/parsing-variables-from-config-file-in-bash
  local LOCALCONF="${1-/usr/local/etc/config_$0}" # set the actual path name of your (DOS or Unix) config file
  if [[ -s $LOCALCONF && -r $LOCALCONF ]]; then
    #tr -d '\r' < $LOCALCONF > $LOCALCONF.unix  # deletes DOS carriage return
    local lhs
    local rhs
    while IFS='= ' read -r lhs rhs; do
      # ! $lhs =~ ^\ *# skips single line comments and -n $lhs skips empty lines
      if [[ ! $lhs =~ ^\ *# && -n $lhs ]]; then
        rhs="${rhs%%\#*}" # Del in line right comments
        #   rhs="${rhs%%*( )}"   # Del trailing spaces, needs shopt -s extglob
        rhs="${rhs%\"*}"         # Del opening string quotes
        rhs="${rhs#\"*}"         # Del closing string quotes
        declare -g "$lhs"="$rhs" # "-g" solo disponible en bash => 4.2
      fi
    done <"$LOCALCONF"
  else
    echo -e "${HL} NO hay configuracion local $NL"
  fi
}

f_existe_comando() {
  #command -v foo >/dev/null 2>&1 || { echo >&2 "I require foo but it's not installed.  Aborting."; exit 1; }
  if command -v "$1" >/dev/null 2>&1; then
    declare "DMCV_$1=0"
    return 0
  else
    declare "DMCV_$1=1"
    return 11
  fi
}

PAQUETE="$1"

if f_existe_comando flatpak; then
  echo -e "${HL} Buscando con FLATPAK $NL"
  #if grep -Ri dl.flathub.org /var/local/ ; then
  if flatpak remote-list | grep -q flathub; then
    flatpak search "$PAQUETE"
  else
    echo -e "${WL}  sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo $NL"
  fi
else
  echo -e "${WL} No tenemos FLATPAK $NL"
fi

if f_existe_comando snap; then
  echo -e "${HL} Buscando con SNAP $NL"
  snap find "$PAQUETE"
else
  echo -e "${WL} No tenemos SNAP $NL"
fi

echo -e "${HL} Buscando con AppImage $NL"
#wget -q -O - https://appimage.github.io/feed.json | grep -i -A 2 "$PAQUETE" | grep -vE "icons|screenshots|^[[:space:]]*$"
if wget -q -O - https://appimage.github.io/feed.json | grep ' "name"' | grep -i "$PAQUETE"; then
  echo -e "${WL}  https://github.com/AppImage/appimage.github.io/search?q=$PAQUETE $NL"
fi

f_get_config /etc/os-release

case "$ID" in
arch)
  echo -e "${HL} Buscando en los repos de Arch: $NL"
  if ! pacman -Ss "$PAQUETE"; then
    echo -e "${WL}  Buscando en los repos de AUR si tenemos paru: $NL"
    f_existe_comando paru && paru -Ss "$PAQUETE"
  fi
  ;;
ubuntu | debian | raspbian)
  if f_existe_comando aptitude; then
    aptitude search "$PAQUETE"
  else
    apt search "$PAQUETE"
  fi
  ;;
opensuse-tumbleweed)
  zypper search -s "$PAQUETE"
  ;;
'')
  echo -e "${WL} SO vacio? raro, raro $NL"
  ;; # allow nothing at all
*)
  echo -e "${WL} No se en que SO estamos $NL"
  ;;
esac

# command -v is POSIX and well defined
# https://stackoverflow.com/questions/592620/how-to-check-if-a-program-exists-from-a-bash-script
# https://unix.stackexchange.com/questions/85249/why-not-use-which-what-to-use-then
