#!/bin/bash
# SPDX-FileCopyrightText: 2021 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# kconfig-hardened-check iterator
# This script generates a a file per each kernel selected with security problems
#set -eEuo pipefail

salvar="$1"
function cleanup {
  rm -rf "$TEMP_DIR"
  echo "Deleted temp working directory $TEMP_DIR"
}
# register the cleanup function to be called on the EXIT signal
[ "$salvar" = "-s" ] || trap cleanup EXIT

TDATE=$(date +'%F')
DIRCAP='/usr/local/share/dmc_hard'
CAPSULE8_CONFS=(CRITICAL HIGH MEDIUM LOW)
TMP_DIR='/tmp/dmc_ch'
TEMP_DIR=$(mktemp -d -t kernel_hardening_XXXXX)
CONFRUNNING="dmc_actual_$TDATE.config"
CONFRUNNINGP="$TEMP_DIR/$CONFRUNNING"
readonly GITARCH='https://git.archlinux.org/svntogit/packages.git/plain/trunk/config?h=packages'
readonly KERNELS=(linux linux-hardened) #linux linux-hardened linux-zen linux-lts

[ -d "$DIRCAP" ] || (
  echo "No encontrado $DIRCAP"
  exit 1
)
[ -d "$TMP_DIR" ] || mkdir "$TMP_DIR"
cd "$TMP_DIR" || exit 2
zcat /proc/config.gz >"$CONFRUNNINGP"
#| tail -n +4

for kernel in "${KERNELS[@]}"; do
  kernelconfig="${kernel}_$TDATE.config"
  [ -s "$kernelconfig" ] || wget -qO- "$GITARCH/$kernel" >"$kernelconfig"
done
#| tail -n +4

#VERBOSE='| tee -a'
DUPCONF='false'
for FILECONFIG in *.config; do
  diff -qs "$FILECONFIG" "$CONFRUNNINGP" && DUPCONF=true
  kconfig-hardened-check -c "$FILECONFIG" >"$TEMP_DIR/${FILECONFIG}_khc"
done
[ "$DUPCONF" = true ] || kconfig-hardened-check -c "$CONFRUNNINGP" >"$TEMP_DIR/${CONFRUNNING}_khc"

cd "$TEMP_DIR" || exit 3
for FILE_KHC in *_khc; do
  for SEVERITY in "${CAPSULE8_CONFS[@]}"; do
    {
      echo "$SEVERITY: $FILE_KHC"
      grep -w -f "$DIRCAP/CAPSULE8_$SEVERITY.kconfig" "$FILE_KHC" | grep 'FAIL:'
      echo ''
    } >>"$FILE_KHC.fail"
    GREPRESTO="$GREPRESTO -f $DIRCAP/CAPSULE8_$SEVERITY.kconfig"
  done
  {
    echo "Resto: $FILE_KHC"
    # shellcheck disable=SC2086
    grep -v -w $GREPRESTO "$FILE_KHC" | grep 'FAIL:'
    echo ''
  } >>"$FILE_KHC.fail"
  GREPRESTO=""
done

if [ "$salvar" = "-s" ]; then
  echo -e "\nLas configuraciones comparables son:"
  find "$TEMP_DIR" -name "*.fail"
  echo "diff -U 1 $TEMP_DIR/*.fail || git -P diff --no-index $TEMP_DIR/*.fail"
else
  diff "$TEMP_DIR"/*.fail || git -P diff --no-index "$TEMP_DIR"/*.fail
fi

# kspp
# https://github.com/linuxkit/linuxkit/tree/master/projects/kspp
# https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project

# grsecurity
# https://grsecurity.net/compare.php
# https://lwn.net/Articles/698827/

# defconfig
# https://stackoverflow.com/questions/41885015/what-exactly-does-linux-kernels-make-defconfig-do

#
# https://capsule8.com/blog/kernel-configuration-glossary/

# https://github.com/a13xp0p0v/kconfig-hardened-check
