#!/bin/bash
# SPDX-FileCopyrightText: 2025 David Marzal
#
# SPDX-License-Identifier: GPL-3.0-only

# Script para buscar paquetes en diferentes distribuciones
#2025-01-29
set -euo pipefail

HL='\033[1;32m' # Bold Green
WL='\033[1;33m' # Bold Yellow
NL='\033[0m'    # Text Reset

CUENTA="${1:-DavidMarzalC}"

#PRE="${TU%@*}"
DPATH="/mnt/XMEDIA/Backups/Manual/Fediverse/$CUENTA"
LPATH="$DPATH/lists"
PREL='mastodonL'
FPATH="$LPATH/$PREL"
GPATH="/home/dmc/git/gitlab/kdeexpress.gitlab.io/static/fedi"

cd "$DPATH" || exit 2

mkdir -p lists && cd lists

# No cnsigo usar variables dentro de ''
awk -F\, '{ print > "mastodonL_" $1 ".csv" }' "$DPATH/lists.csv"

#Cuentas a añadir a mano
Manuales="
Podcast,angelescustodios@papafrikifeed.duckdns.org
Podcast,cavalleto@masto.es
Podcast,djkibou@mastodon.social
Podcast,entreletraspod@bookstodon.es
Podcast,hermesgabriel@todon.nl
Podcast,Jonybcn@mastodon.social
Podcast,lynze@mastodon.social
Podcast,martatrivi@mastodon.social
Podcast,mirindo@mastodon.social
Podcast,ocularis@mastodon.nu
Podcast,palangano_media@oye.social
Podcast,proteusbcn@mastodon.nu
Podcast,Rocquito@social.linux.pizza
Podcast,tucafeprivado@podcast.theprivatecoffee.nohost.me
"

for i in $Manuales; do
    l_flist="${FPATH}_$(echo $i | cut -d "," -f1).csv"
    grep -x "$i" "$l_flist" || echo "$i" >> "$l_flist";
done

case $CUENTA in
  DavidMarzalC)
    TU='DavidMarzalC@masto.es'
    pubexc="EuropeanUnion MediosComunicacion"
    publicados="Accesibilidad BetterPlanet Podcast RMurcia Vegan"
    ;;
  KDE)
    TU='kde_espana@floss.social'
    pubexc="KDE_int Colaboradores"
    publicados="KDE Socies Komunidad"
    # https://www.kde-espana.org/quienes-somos
    ;;
  *) echo "Unknown: $CUENTA"
    return 1
    ;;
esac


# Copiamos las listas en las que NO nos incluimos
for i in $pubexc; do
    l_ffile="${PREL}_${CUENTA}_$i.csv"
    cp -f "${FPATH}_$i.csv" "$GPATH/$l_ffile"
done
# Copiamos las listas en las que SI nos incluimos
for i in $publicados; do
    l_ffile="${PREL}_${CUENTA}_$i.csv"
    grep -x "${i},$TU" "${FPATH}_$i.csv" || echo "${i},$TU" >> "${FPATH}_$i.csv" 
    cp -f "${FPATH}_$i.csv" "$GPATH/$l_ffile"
done

# Por retrocompatibilidad
[ "$CUENTA" = "DavidMarzalC" ] && cp -f "${FPATH}_Podcast.csv" $GPATH/mastodon_list_podcasts.csv

echo -e "${HL}Fin correcto ${NL}"