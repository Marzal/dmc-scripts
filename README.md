
# DMC Scripts
Conjunto de utilidades en bash para facilitar el uso de la consola

## bin

### [dmc_bashelper.sh](bin/dmc_bashelper.sh)
Conjunto de funciones para ser importadas en un script principal

### [dmc_buscapp.sh](bin/dmc_buscapp.sh)
Buscar paquetes en diferentes distribuciones

### [dmc_check_lhardening.sh](bin/dmc_check_lhardening.sh)
This script generates a file per each kernel selected with security problems

### [dmc_checksh.sh](bin/dmc_checksh.sh)
Revisar con shellharden los scripts .sh de /usr/local/{bin,sbin}

### [dmc_ddxorg.sh](bin/dmc_ddxorg.sh)
Mostrar información de stack gŕafico X11/Wayland

### [dmc_fflufs.sh](bin/dmc_fflufs.sh)
Wrapper para normalizar audio con loudnorm de ffmpeg. Un poco de info [teórica](https://marzal.gitlab.io/mundolibre/docs/multimedia/audacity/) y [práctica](https://marzal.gitlab.io/mundolibre/docs/multimedia/ffmpeg/#normalizaci%C3%B3n)

### [dmc_flatatom.sh](bin/dmc_flatatom.sh)
Lanzar shellcheck en un "jaula" de flatpak

### [dmc_peertube.sh](bin/dmc_peertube.sh)
Subir ficheros a Peertube con metadatos. [Documentación](doc/dmc_peertube.md)

### [dmc_waylandroot.sh](bin/dmc_waylandroot.sh)
Habilitar la ejecucióón de root en Wayland para ciertas aplicaciones

### [dmc_ddxorg.sh](bin/dmc_ddxorg.sh)
Muestra la info de X o Wayland

### [dmc_mastosplit.sh](bin/dmc_mastosplit.sh)
Convierte la exportación de tus listas de Mastodon a un fichero por cada lista

### [dmc_wfix.sh](bin/dmc_wfix.sh)
Arreglar errores comunes de las traducciones de Whisper

### [dmc_txtx2.sh](bin/dmc_txtx2.sh)
Transforma la transcripción de Whisper de 32 caracteres a el doble para integrar en webs

# sbin
