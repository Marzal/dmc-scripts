# [dmc_peertube.sh](../bin/dmc_peertube.sh)
Script para poder subir ficheros a Peertube con metadatos

## Requisitos
* Tener descargado el *helper* [dmc_bashelper.sh](../bin/dmc_bashelper.sh)
    * Si se clona el repositorio ya va todo lo necesario
    * Otra opción es descargar solo dmc_bashelper.sh y dmc_peertube.sh. Por ejemplo con `wget https://gitlab.com/Marzal/dmc-scripts/-/raw/master/bin/dmc_{bashelper,peertube}.sh`
* Crear un fichero de configuración (ejemplo)
* `jq`:
    * [Arch](https://archlinux.org/packages/community/x86_64/jq/) (y derivados): `pacman -S jq`

## Configuración

* Ruta para el fichero: `$HOME/.config/peertube-cli/peertube_$CUENTA.conf`, donde `$CUENTA` es el nombre que le quieras dar a la plantilla/profile (puedes tener los que quieras).
* El fichero tiene que tener estás opciones:
```bash
USERNAME=USUARIO
PASSWORD=CONTRASEÑA
API_PATH="https://URL.INSTANCIA.PEERTUBE/api/v1"
TAG1=ETIQUETA1
TAG2=LUEGO
TAG3=CAMBIABLES
TAG4=MAXIMO4
LICENCIA=2
```
* Parametros:
    * **USERNAME**: Usuario de la cuenta a la que se va a subir.
    * **PASSWORD**: La contraseña para autentificarse.
    * **API_PATH**: URL del API de vuestra instancia de PEERTUBE.
    * **TAG[1-4]**: Las etiquetas genericas a usar. Una vez subido el video se pueden cambiar, aquí poner las más usadas que es más rápido borrarlas luego que añadiras.
    * **LICENCIA**: ID de la licencia CC que tendrá el vídeo. Por defecto es la 2 CC-BY-SA. Ver opciones con `-L`.

## Uso
* Hay que lanzar el comando con el primer operador -a/--account obligatoriamente. Donde 
* Operadores:
    * INFO
        * **-C**: Muestra el ID de los canales que tiene nuestra cuenta, que nos hara falta para poder subir vídeos.
        * **-L**: Muestra el ID de las licencias que se pueden usar en Peertube. Configurable en la plantilla.
        * **-h**: Muestra la ayuda.
        * **-V**: Muestra la versión del script.
    * OBLIGATORIO
        * **-a**: El parametro a poner es la plantilla/cuenta que queremos usar. El `$CUENTA` que usamos para el fichero de configuración. Siempre va primero.
    * OBLIGATORIO PARA SUBIR UN VÍDEO:
        * **-u**: RUTA del vídeo que queremos subir.
        * **-c**: ID sacada con el `-C` del canal al que queremos subir el vídeo.
    * OPCIONALES
        * **-d**: Descripción del vídeo.

* Ejecutando el comando  con `-h`, `--help` o sin parametros muestra la ayuda.

```bash
Usage: peertube.sh -a profile [OPTIONS]

 -a | --account               - Peertube's profile - Mandatory first
 -c | --channel               - Channel to work with
 -d | --desc                  - Video description
 -u | --upload                - video to upload
 -V | --version               - print version of this script
 -h | --help                  - print this help
 -C | --canales               - Get channels from ACCOUNT
 -L | --licencias             - Get licences avaiables
-a PROFILE is mandatory as first parameter
```